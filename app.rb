module App
  class API < Sinatra::Base
    configure do
      set :config, settings.root + '/config'
    end

    configure :development do
      yaml_data = YAML.load_file("#{settings.config}/db.yml")
      db_data = yaml_data['development']

      dbtype = ENV['DBTYPE'] || db_data['type']
      dbuser = ENV['DBUSER'] || db_data['user']
      dbhost = ENV['DBHOST'] || db_data['host']
      dbport = ENV['DBPORT'] || db_data['port']
      dbname = ENV['DBNAME'] || db_data['name']

      set :dburl, "#{dbtype}://#{dbuser}@#{dbhost}:#{dbport}/#{dbname}"
    end

    configure :production do
      yaml_data = YAML.load_file("#{settings.config}/db.yml")
      db_data = yaml_data['development']

      dbtype = ENV['DBTYPE'] || db_data['type']
      dbuser = ENV['DBUSER'] || db_data['user']
      dbhost = ENV['DBHOST'] || db_data['host']
      dbport = ENV['DBPORT'] || db_data['port']
      dbname = ENV['DBNAME'] || db_data['name']

      set :dburl, "#{dbtype}://#{dbuser}@#{dbhost}:#{dbport}/#{dbname}"
    end

    configure :development, :production do
      set :bind, '0.0.0.0'
      set :port, 5000

      enable :logging
    end

    configure :development do
      enable :cross_origin
      set :allow_origin, :any
      set :allow_methods, %i[get post options]
      set :allow_credentials, true
      set :max_age, '1728000'
      set :expose_headers, ['Content-Type']

      set :show_exceptions, :after_handler
    end

    not_found do
      'resource not found'
    end

    error do
      'error'
    end

    options '*' do
    end

    get '/graphql/?' do
      content_type 'application/json'

      query = GraphQL::Introspection::INTROSPECTION_QUERY
      results = GraphQL::Types::Rubygems::Schema.execute(query)

      status 200
      body JSON.pretty_generate(results)
    end

    post '/graphql/?' do
      query = ''

      case request.content_type
      when 'application/graphql'
        query = request.body.read
      when 'application/json'
        body = request.body.read
        json = JSON.parse(body)
        query = json['query']
      else
        raise Sinatra::NotFound
      end

      results = GraphQL::Types::Rubygems::Schema.execute(query)

      content_type 'application/json'
      status 200
      body JSON.pretty_generate(results)
    end

    run! if app_file == $PROGRAM_NAME
  end
end
