FROM mswinson/ruby-pg:latest

ADD . /var/services/rubygems-graphql
WORKDIR /var/services/rubygems-graphql

RUN ./scripts/setup
CMD ./scripts/init
