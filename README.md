# rubygems-graphql

GraphQL API for the Rubygems database

## Prequisites

      docker
      docker-compose
      a running postgres instance, loaded with the rubygems database dump

## Usage

      docker run mswinson/rubygems-graphql:develop

## Configuration

ports

      5000

environment variables

      DBHOST    # postgres hostname, default 'localhost'
      DBPORT    # postgres port, default 5432
      DBUSER    # postgres user name , default 'postgres'
      DBNAME    # postgres database name, default 'rubygems'


## Authors

      Mark Swinson < mark@mswinson.com >

## Contributing

1. Fork it ( http://bitbucket.org/mswinson-labs/rubygems-graphql/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
