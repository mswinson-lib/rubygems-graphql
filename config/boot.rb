require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/rubygems'
require 'json'
require 'yaml'
require './app'

dburl = App::API.settings.dburl

GraphQL::Schemas::Rubygems.configure do |config|
  config.url = dburl
end
